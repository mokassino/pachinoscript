#!/bin/sh

cflag=0

# Check audio files
wtime_audio="/home/kurush/downloads/audio/arcade.mp3"
ptime_audio="/home/kurush/downloads/audio/MetalSlug-MissionComplete.mp3"

if ( [[ $wtime_audio == "" ]] || [[ $ptime_audio == "" ]] ); then
        echo "Error audio files to play not found" && exit 1
fi

if ( [[ $wtime_audio != *.mp3 ]] || [[ $ptime_audio != *.mp3 ]] ); then
        echo "Error audio files must be wav files" && exit 2
fi

# Check input parameters
wtime=25
ptime=5
rep=1

if  [[ $1 == '-c' ]] ; then
        cflag=1;
        [[ $2 == ?(-)+([0-9]) ]] && [ $2 -gt 0 ] && rep=$2
        [[ $3 == ?(-)+([0-9]) ]] && [ $3 -gt 0 ] && wtime=$3
        [[ $4 == ?(-)+([0-9]) ]] && [ $4 -gt 0 ] && ptime=$4
        dummywtime=$3
        dummyptime=$4
elif  [[ $1 =~ ^-.* ]]; then #case you mistype the input, like pachino -x
        echo "Input error" && exit 3
else
        [[ $1 == ?(-)+([0-9]) ]] && [ $1 -gt 0 ] && rep=$1
        [[ $2 == ?(-)+([0-9]) ]] && [ $2 -gt 0 ] && wtime=$2
        [[ $3 == ?(-)+([0-9]) ]] && [ $3 -gt 0 ] && ptime=$3
fi


# Test if you can play them
`mpg123 -qt $ptime_audio 2>/dev/null`
if [ $? -eq 1 ]; then
        echo "Error ptime_audio isn't a path to a playable sound file" && exit 4
fi

`mpg123 -q $wtime_audio 2>/dev/null`
if [ $? -eq 1 ]; then
        echo "Error wtime_audio isn't a path to a playable sound file" && exit 4
fi

# Pomodoro
if ( [[ $cflag == 0 ]] ); then #without input -c
        while [ $rep -gt 0 ]
        do
                `sleep ${wtime}m`
                `mpg123 -q $ptime_audio 2>/dev/null`

                `sleep ${ptime}m`
                ((rep--))
                `mpg123 -q $wtime_audio 2>/dev/null`
        done
else # with input -c
        while [ $rep -gt 0 ]
        do
                let "dummywtime = wtime * 60"
                while [ $dummywtime -gt 0 ]
                do
                        ((dummywtime--))
                        echo -ne " FOCUS! $(date -ud "@$dummywtime" +"%M:%S")\033[0K\r"
                        sleep 1
                done
                `mpg123 -q $ptime_audio 2>/dev/null`

                let "dummyptime = ptime * 60"
                while [ $dummyptime -gt 0 ]
                do
                        ((dummyptime--))
                        echo -ne " CHILL $(date -ud "@$dummyptime" +"%M:%S")\033[0K\r"
                        sleep 1
                done
                `mpg123 -q $wtime_audio 2>/dev/null`
                ((rep --))

        done
fi

exit 0
